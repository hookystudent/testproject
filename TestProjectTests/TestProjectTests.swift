//
//  TestProjectTests.swift
//  TestProjectTests
//
//  Created by Admin on 2020/11/3.
//  Copyright © 2020 HULIN. All rights reserved.
//

import XCTest
@testable import TestProject

class TestProjectTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
//        XCTFail("this is a fail test");  // 生成一个失败的测试


    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testRequst() throws {
        let xctest:XCTestExpectation = self.expectation(description: "apitest")
        if let url = URL.init(string: "https://api.github.com/") {
        let reuqest:URLRequest = URLRequest.init(url: url)
        NSURLConnection.sendAsynchronousRequest(reuqest , queue: OperationQueue()) {(res:URLResponse?, data:Data?, error:Error!) -> Void in
            if let d = data{
                let getMsg = NSString(data: d, encoding: String.Encoding.utf8.rawValue)! as String
                xctest.fulfill()
                print(getMsg)
            }
            
            if (error != nil){
                print("error：",error.localizedDescription)
            }
        }
        
        }
        
        self.waitForExpectations(timeout: 15) { (error) in
            print("Expectation:",error?.localizedDescription ?? "")
        }
        
    }

}
