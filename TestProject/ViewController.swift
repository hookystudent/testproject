//
//  ViewController.swift
//  TestProject
//
//  Created by Admin on 2020/11/3.
//  Copyright © 2020 HULIN. All rights reserved.
//

import UIKit

let HLResluts = "HLReslutsKey"
let HLTimes = "HLTimesKey"

class ViewController: UIViewController {

    @IBOutlet var txtResults:UITextView!
    var content:String = ""
    var times:NSInteger = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        times = UserDefaults.standard.integer(forKey: HLTimes)
        content = UserDefaults.standard.string(forKey: HLResluts) ?? ""
        print("content:",content)
        self.refreshTxt()
        Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(doRequest), userInfo: nil, repeats: true)
    }
    
    @objc func doRequest(){
        if let url = URL.init(string: "https://api.github.com/") {
        let reuqest:URLRequest = URLRequest.init(url: url)
        NSURLConnection.sendAsynchronousRequest(reuqest , queue: OperationQueue()) {(res:URLResponse?, data:Data?, error:Error!) -> Void in
            if let d = data{
                let getMsg = NSString(data: d, encoding: String.Encoding.utf8.rawValue)! as String
                if self.content.count == 0 {
                    self.content = "\(self.times)" + ":" + "\(String(describing: getMsg))"
                }
                else{
                    self.content = "\(self.content)" + "\n\n\n" + "\(self.times)" + ":" + "\(String(describing: getMsg))"
                }
                self.times += 1
                UserDefaults.standard.set(self.content, forKey: HLResluts)
                UserDefaults.standard.set(self.times, forKey: HLTimes)
                UserDefaults.standard.synchronize()
                self.performSelector(onMainThread: #selector(self.refreshTxt), with: nil, waitUntilDone: true)
                print(self.content)

            }
            
            if (error != nil){
                print("error：",error ?? "")
                if self.content.count == 0 {
                    self.content = "\(self.times)" + ":" + "\(error.localizedDescription)"
                }
                else{
                    self.content = "\(self.content)" + "\n\n\n" + "\(self.times)" + ":" + "\(error.localizedDescription)"
                }
                self.times += 1
                
                UserDefaults.standard.set(self.content, forKey: HLResluts)
                UserDefaults.standard.set(self.times, forKey: HLTimes)
                UserDefaults.standard.synchronize()
                self.performSelector(onMainThread: #selector(self.refreshTxt), with: nil, waitUntilDone: true)
            }
        }
        
        }
    }
    
    @objc func refreshTxt() {
        txtResults.text = content
        txtResults.scrollRangeToVisible(NSMakeRange(txtResults.text.count, 1))
    }
    
    @IBAction func clear(){
        content = ""
        times = 0
        UserDefaults.standard.set(content, forKey: HLResluts)
        UserDefaults.standard.set(times, forKey: HLTimes)
        UserDefaults.standard.synchronize()
        txtResults.text = ""
    }
}

